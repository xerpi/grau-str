#include<Wire.h>
const int MPU = 0x68;  // I2C address of the MPU-6050
int16_t AcX,AcY,AcZ,Tmp,GyX,GyY,GyZ;

static inline void serial_write_int16(int16_t i)
{
	Serial.write((byte *)&i, sizeof(i));
}

void setup()
{
	pinMode(LED_BUILTIN, OUTPUT);
	digitalWrite(LED_BUILTIN, LOW);

	Serial.begin(9600);

	// Esperem a que el PC ens avisi
	while (Serial.available() == 0)
		delay(100);

	Wire.begin();
	Wire.beginTransmission(MPU);
	Wire.write(0x6B);  // PWR_MGMT_1 register
	Wire.write(0);     // set to zero (wakes up the MPU-6050)
	Wire.endTransmission();

	digitalWrite(LED_BUILTIN, HIGH);
}

void loop()
{
	Wire.beginTransmission(MPU);
	Wire.write(0x3B);  // starting with register 0x3B (ACCEL_XOUT_H)
	Wire.endTransmission(false);
	Wire.requestFrom(MPU,14,true);  // request a total of 14 registers
	
	AcX=Wire.read()<<8|Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
	AcY=Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
	AcZ=Wire.read()<<8|Wire.read();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
	Tmp=Wire.read()<<8|Wire.read();  // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
	GyX=Wire.read()<<8|Wire.read();  // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
	GyY=Wire.read()<<8|Wire.read();  // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
	GyZ=Wire.read()<<8|Wire.read();  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)

	serial_write_int16(AcX);
	serial_write_int16(AcY);
	serial_write_int16(AcZ);
	serial_write_int16(GyX);
	serial_write_int16(GyY);
	serial_write_int16(GyZ);
	serial_write_int16(Tmp);
	Serial.flush();

	delay(50);
}




