#include <Wire.h>

static const int MPU = 0x68;	// I2C address of the MPU-6050

static int sampling_delay;

static inline void serial_write_int16(int16_t i)
{
	Serial.write((byte *)&i, sizeof(i));
}

static inline byte serial_read_byte()
{
	byte b;

	while (Serial.available() < 1) ;

	Serial.readBytes((byte *)&b, sizeof(b));

	return b;
}

void setup()
{
	pinMode(LED_BUILTIN, OUTPUT);
	digitalWrite(LED_BUILTIN, LOW);

	Serial.begin(115200);

	// Esperem a que el PC ens avisi
	byte gyro_sel = serial_read_byte();
	byte accel_sel = serial_read_byte();
	byte sampling_freq = serial_read_byte();
	sampling_delay = 1000 / sampling_freq;

	Wire.begin();
	Wire.beginTransmission(MPU);
	Wire.write(0x6B);	// PWR_MGMT_1 register
	Wire.write(0);		// set to zero (wakes up the MPU-6050)
	Wire.endTransmission();

	/* Read accelerometer config */
	Wire.beginTransmission(MPU);
	Wire.write(0x1C);
	Wire.endTransmission();
	Wire.requestFrom(MPU, 1, true);
	byte accel_cfg = Wire.read();

	/* Write accelerometer config */
	Wire.beginTransmission(MPU);
	Wire.write(0x1C);
	Wire.write((accel_cfg & ~(3 << 3)) | accel_sel << 3);
	Wire.endTransmission();

	/* Read gyro config */
	Wire.beginTransmission(MPU);
	Wire.write(0x1B);
	Wire.endTransmission();
	Wire.requestFrom(MPU, 1, true);
	byte gyro_cfg = Wire.read();

	/* Write gyro config */
	Wire.beginTransmission(MPU);
	Wire.write(0x1B);
	Wire.write((gyro_cfg & ~(3 << 3)) | gyro_sel << 3);
	Wire.endTransmission();

	digitalWrite(LED_BUILTIN, HIGH);
}

void loop()
{
	int16_t acc_x, acc_y, acc_z, temp, gyro_x, gyro_y, gyro_z;

	Wire.beginTransmission(MPU);
	Wire.write(0x3B);			// starting with register 0x3B (ACCEL_XOUT_H)
	Wire.endTransmission(false);
	Wire.requestFrom(MPU, 14, true);	// request a total of 14 registers

	acc_x = Wire.read() << 8 | Wire.read();		// 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
	acc_y = Wire.read() << 8 | Wire.read();		// 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
	acc_z = Wire.read() << 8 | Wire.read();		// 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
	temp = Wire.read() << 8 | Wire.read();		// 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
	gyro_x = Wire.read() << 8 | Wire.read();	// 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
	gyro_y = Wire.read() << 8 | Wire.read();	// 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
	gyro_z = Wire.read() << 8 | Wire.read();	// 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)

	serial_write_int16(acc_x);
	serial_write_int16(acc_y);
	serial_write_int16(acc_z);
	serial_write_int16(gyro_x);
	serial_write_int16(gyro_y);
	serial_write_int16(gyro_z);
	serial_write_int16(temp);
	Serial.flush();

	delay(sampling_delay);
}
