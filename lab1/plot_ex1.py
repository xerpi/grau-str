#-*- coding: utf-8 -*-
import random
import time
import struct
import serial
from collections import deque
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import numpy as np
import os

ser = serial.Serial('/dev/ttyACM0',
	baudrate = 9600,
	timeout = None)

ser.flushInput()
# Li diem al Arduino que comenci
for i in range(512):
	ser.write(bytes(0x01))
ser.flushOutput()

win = pg.GraphicsWindow()
win.setWindowTitle('STR - Lab 1')

p1 = win.addPlot()
p1.setLabel(axis = "left", text = "Acceleration (m/s^2)")
p1.setLabel(axis = "bottom", text = "Time (s)")
p2 = win.addPlot()
p2.setLabel(axis = "left", text = "Angular velocity (º/s)")
p2.setLabel(axis = "bottom", text = "Time (s)")
p3 = win.addPlot()
p3.setLabel(axis = "left", text = "Temperature (ºC)")
p3.setLabel(axis = "bottom", text = "Time (s)")

curve_acc_x = p1.plot()
curve_acc_y = p1.plot()
curve_acc_z = p1.plot()
curve_gyro_x = p2.plot()
curve_gyro_y = p2.plot()
curve_gyro_z = p2.plot()
curve_temp = p3.plot()

data_acc_x = []
data_acc_y = []
data_acc_z = []
data_gyro_x = []
data_gyro_y = []
data_gyro_z = []
data_temp = []

range_values_acc = [0.0, 0.0]
range_values_gyro = [0.0, 0.0]
range_values_temp = [0.0, 0.0]
t = 0.0

def serial_readint16():
	data = ser.read(2)
	f, = struct.unpack('<h', data)
	return f


def add_element(array, t, val, range_values):
	array.append((t, val))

def acc_data_to_ms2(val):
	return (val / 16384.0) * 9.80665

def gyro_data_to_degs(val):
	return (val / 131.0)


def update_data():
	global t

	add_element(data_acc_x, t, acc_data_to_ms2(serial_readint16()), range_values_acc)
	add_element(data_acc_y, t, acc_data_to_ms2(serial_readint16()), range_values_acc)
	add_element(data_acc_z, t, acc_data_to_ms2(serial_readint16()), range_values_acc)
	add_element(data_gyro_x, t, gyro_data_to_degs(serial_readint16()), range_values_gyro)
	add_element(data_gyro_y, t, gyro_data_to_degs(serial_readint16()), range_values_gyro)
	add_element(data_gyro_z, t, gyro_data_to_degs(serial_readint16()), range_values_gyro)
	add_element(data_temp, t, serial_readint16()/340.00+36.53, range_values_temp)

	t = t + 1.0
	p1.autoRange()
	p1.setXRange(t - 20, t)
	p1.setYRange(-19.6, 19.6)
	p2.autoRange()
	p2.setXRange(t - 20, t)
	p2.setYRange(-720, 720)
	p3.autoRange()
	p3.setXRange(t - 20, t)
	p3.setYRange(10, 30)

def update_plot():
	# Accelerometer
	curve_acc_x.setData(np.array(data_acc_x), pen = pg.mkPen(color=(255, 0, 0), style=QtCore.Qt.SolidLine))
	curve_acc_y.setData(np.array(data_acc_y), pen = pg.mkPen(color=(0, 255, 0), style=QtCore.Qt.SolidLine))
	curve_acc_z.setData(np.array(data_acc_z), pen = pg.mkPen(color=(0, 0, 255), style=QtCore.Qt.SolidLine))
	# Gyroscope
	curve_gyro_x.setData(np.array(data_gyro_x), pen = pg.mkPen(color=(255, 0, 0), style=QtCore.Qt.SolidLine))
	curve_gyro_y.setData(np.array(data_gyro_y), pen = pg.mkPen(color=(0, 255, 0), style=QtCore.Qt.SolidLine))
	curve_gyro_z.setData(np.array(data_gyro_z), pen = pg.mkPen(color=(0, 0, 255), style=QtCore.Qt.SolidLine))
	# Temperature
	curve_temp.setData(np.array(data_temp), pen = pg.mkPen(color=(255, 0, 0), style=QtCore.Qt.SolidLine))


def update():
	update_data()
	update_plot()

timer = pg.QtCore.QTimer()
timer.timeout.connect(update)
timer.start(50)

## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
	import sys
	if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
		QtGui.QApplication.instance().exec_()# -*- coding: utf-8 -*-

ser.close()
