#-*- coding: utf-8 -*-
import sys
import random
import math
import time
import struct
import serial
import collections
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import numpy as np
import os

GRAVITY = 9.80665

# Parametres
gyro_sel = 0
acc_sel = 0
sampling_freq = 10

ser = serial.Serial('/dev/ttyACM0',
	baudrate = 115200,
	timeout = None)

# Toggle DTR to reset Arduino
ser.setDTR(False)
ser.flushInput()
ser.setDTR(True)
time.sleep(2)

# Enviem els parametres al Arduino
ser.write(bytes([gyro_sel, acc_sel, sampling_freq]))

def serial_readint16():
	data = ser.read(2)
	f, = struct.unpack('<h', data)
	return f

# From the datasheet
def acc_range_from_sel(sel):
	return 2 ** (sel + 1)

def gyro_range_from_sel(sel):
	return 250 * (2 ** sel)

def acc_sens_from_sel(sel):
	return 16384.0 / (2 ** sel)

def gyro_sens_from_sel(sel):
	return 131 / (2 ** sel)

def acc_data_to_ms2(val, sel):
	return (val / acc_sens_from_sel(sel)) * GRAVITY

def gyro_data_to_degs(val, sel):
	return val / gyro_sens_from_sel(sel)

def temp_data_to_celsius(val):
	return val / 340.00 + 36.53

class DynamicPlotter():
	def __init__(self, sampleinterval=0.1, timewindow=10.0):
		# Data stuff
		self.interval = sampleinterval
		self.timewindow = timewindow

		# PyQtGraph stuff
		self.app = QtGui.QApplication([])

		self.win = pg.GraphicsWindow()
		self.win.setWindowTitle('STR - Lab 1')
		self.plt1 = self.win.addPlot()
		self.plt1.setLabel(axis = "left", text = "Acceleration (m/s^2)")
		self.plt1.setLabel(axis = "bottom", text = "Time (s)")
		self.plt2 = self.win.addPlot()
		self.plt2.setLabel(axis = "left", text = "Angular velocity (º/s)")
		self.plt2.setLabel(axis = "bottom", text = "Time (s)")
		self.plt3 = self.win.addPlot()
		self.plt3.setLabel(axis = "left", text = "Temperature (ºC)")
		self.plt3.setLabel(axis = "bottom", text = "Time (s)")
		self.curve_acc_x = self.plt1.plot()
		self.curve_acc_y = self.plt1.plot()
		self.curve_acc_z = self.plt1.plot()
		self.curve_gyro_x = self.plt2.plot()
		self.curve_gyro_y = self.plt2.plot()
		self.curve_gyro_z = self.plt2.plot()
		self.curve_temp = self.plt3.plot()

		# Init variables
		self.time = 0.0
		self.data_acc_x = []
		self.data_acc_y = []
		self.data_acc_z = []
		self.data_gyro_x = []
		self.data_gyro_y = []
		self.data_gyro_z = []
		self.data_temp = []

		# QTimer
		self.timer = QtCore.QTimer()
		self.timer.timeout.connect(self.updateplot)
		self.timer.start(self.interval * 1000)

	def add_sample(self, array, val):
		array.append((self.time, val))

	def fetchdata(self):
		self.add_sample(self.data_acc_x, acc_data_to_ms2(serial_readint16(), acc_sel))
		self.add_sample(self.data_acc_y, acc_data_to_ms2(serial_readint16(), acc_sel))
		self.add_sample(self.data_acc_z, acc_data_to_ms2(serial_readint16(), acc_sel))
		self.add_sample(self.data_gyro_x, gyro_data_to_degs(serial_readint16(), gyro_sel))
		self.add_sample(self.data_gyro_y, gyro_data_to_degs(serial_readint16(), gyro_sel))
		self.add_sample(self.data_gyro_z, gyro_data_to_degs(serial_readint16(), gyro_sel))
		self.add_sample(self.data_temp, temp_data_to_celsius(serial_readint16()))

	def updatedata(self):
		self.fetchdata()

		# Accelerometer
		self.curve_acc_x.setData(np.array(self.data_acc_x), pen = pg.mkPen(color=(255, 0, 0), style=QtCore.Qt.SolidLine))
		self.curve_acc_y.setData(np.array(self.data_acc_y), pen = pg.mkPen(color=(0, 255, 0), style=QtCore.Qt.SolidLine))
		self.curve_acc_z.setData(np.array(self.data_acc_z), pen = pg.mkPen(color=(0, 0, 255), style=QtCore.Qt.SolidLine))
		# Gyroscope
		self.curve_gyro_x.setData(np.array(self.data_gyro_x), pen = pg.mkPen(color=(255, 0, 0), style=QtCore.Qt.SolidLine))
		self.curve_gyro_y.setData(np.array(self.data_gyro_y), pen = pg.mkPen(color=(0, 255, 0), style=QtCore.Qt.SolidLine))
		self.curve_gyro_z.setData(np.array(self.data_gyro_z), pen = pg.mkPen(color=(0, 0, 255), style=QtCore.Qt.SolidLine))
		# Temperature
		self.curve_temp.setData(np.array(self.data_temp), pen = pg.mkPen(color=(255, 0, 0), style=QtCore.Qt.SolidLine))

		# Actualitzar 'live' plot
		self.plt1.autoRange()
		self.plt2.autoRange()
		self.plt3.autoRange()
		self.plt1.setXRange(self.time - self.timewindow, self.time)
		self.plt2.setXRange(self.time - self.timewindow, self.time)
		self.plt3.setXRange(self.time - self.timewindow, self.time)

		# Rang de les mostres
		acc_range = acc_range_from_sel(acc_sel) * GRAVITY
		gyro_range = gyro_range_from_sel(gyro_sel)

		self.plt1.setYRange(-acc_range, acc_range)
		self.plt2.setYRange(-gyro_range, gyro_range)
		self.plt3.setYRange(10, 30)

		# Update time
		self.time = self.time + self.interval

	def updateplot(self):
		self.updatedata()
		self.app.processEvents()

	def run(self):
		self.app.exec_()

if __name__ == '__main__':
	interval = (1.0 / sampling_freq)
	plot = DynamicPlotter(sampleinterval=interval, timewindow=(interval * 5))
	plot.run()
	ser.close()
