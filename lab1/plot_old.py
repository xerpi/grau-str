import numpy as np
import matplotlib.pyplot as plt
import serial
import io
import struct
import time

ser = serial.Serial('/dev/ttyACM0',
	baudrate = 9600,
	timeout=None)

ser.flushInput()
while True:
	if ser.inWaiting() > 0:
		c = ser.read()
		if c == '<':
			break

def readline_serial():
	while True:
		ser.flushInput()
		state = ser.readline()
		if state == "":
			continue
		return state

def readfloat():
	while True:
		if ser.inWaiting() > 0:
			c = ser.read()
			if c == '<':
				break
	value = ''
	while True:
		if ser.inWaiting() > 0:
			c = ser.read()
			if c == '>':
				break
			value = value + c

	return value

x = []
y = []

for i in range(20):
	#val = readline_serial()
	val = readfloat()
	print(val)
	x.append(i)
	y.append(float(val))


ser.close()

print(y)

plt.plot(x, y)
plt.show()


