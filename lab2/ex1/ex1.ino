#include <Wire.h>

#define RPM_TO_PWM(x) (((x) * 255ull) / 220)

static const int ENA = 6;
static const int IN1 = 7;
static const int IN2 = 8; //as before
static const int encoderB = 10;
static const int encoderA = 11; //connected to Arduino's ports 2 and 3
static int target_RPM = 150;

static int read_rpm_blocking()
{
	while (Serial.available() == 0)
		delay(50);

	return Serial.parseInt();
}

void setup()
{
	pinMode(ENA, OUTPUT);
	pinMode(IN1, OUTPUT);
	pinMode(IN2, OUTPUT); //outputs

	pinMode(encoderA, INPUT);
	pinMode(encoderB, INPUT); //inputs

	digitalWrite(IN1, HIGH);
	digitalWrite(IN2, LOW);//setting motorA's direction

	Serial.begin(115200);
	target_RPM = read_rpm_blocking();

	analogWrite(ENA, RPM_TO_PWM(target_RPM));//start driving motorA
	digitalWrite(encoderB, HIGH);//start driving motorA
}

void loop()
{
	if (Serial.available() > 0) {
		target_RPM = Serial.parseInt();
		analogWrite(ENA, RPM_TO_PWM(target_RPM));
	}

	int16_t pulseBlength = pulseIn(encoderB, HIGH);

	Serial.println(pulseBlength);
	Serial.flush();

	// Delay 20ms
	delay(20);
}
