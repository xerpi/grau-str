#include <Wire.h>

#define RPM_TO_PWM(x) (((x) * 255ul) / 220)
#define PULSELENGTH_TO_RPM(x) (1000000ul / ((x) * 3ul))

static const int ENA = 6;
static const int IN1 = 7;
static const int IN2 = 8; //as before
static const int encoderB = 10;
static const int encoderA = 11; //connected to Arduino's ports 2 and 3
static long target_rpm = 150;
static long sampling_period = 20;

// Timekeeping
static unsigned long last_millis = 0;
// PID values
static float Kp = 0.5f;
static float Ki = 0.5f;
// Accumulated error
static long accumulated_error = 0;

static int read_int_blocking()
{
	while (Serial.available() == 0)
		delay(50);

	return Serial.parseInt();
}

void setup()
{
	pinMode(ENA, OUTPUT);
	pinMode(IN1, OUTPUT);
	pinMode(IN2, OUTPUT); //outputs

	pinMode(encoderA, INPUT);
	pinMode(encoderB, INPUT); //inputs

	digitalWrite(IN1, HIGH);
	digitalWrite(IN2, LOW);//setting motorA's direction

	Serial.begin(115200);
	target_rpm = read_int_blocking();
	sampling_period = read_int_blocking();

	analogWrite(ENA, RPM_TO_PWM(target_rpm)); //start driving motorA
	digitalWrite(encoderB, HIGH); //start driving motorA

	last_millis = millis();
}

void loop()
{
	unsigned long cur_millis;
	unsigned long delta_millis;
	unsigned long pulse_length;
	unsigned long current_rpm;
	long error_rpm;
	unsigned long pid_rpm;

	// Read desired RPMs from PC
	if (Serial.available() > 0) {
		target_rpm = Serial.parseInt();
		sampling_period = Serial.parseInt();
	}

	// Delta time between now and last iteration
	cur_millis = millis();
	delta_millis = cur_millis - last_millis;
	last_millis = cur_millis;

	// Read motor's RPM
	pulse_length = pulseIn(encoderB, HIGH);
	current_rpm = PULSELENGTH_TO_RPM(pulse_length);

	// PID controller
	error_rpm = (long)target_rpm - current_rpm;
	accumulated_error += error_rpm * delta_millis;
	pid_rpm = Kp * error_rpm + Ki * (accumulated_error / 10.0f);

	// Write RPM correction to motor
	analogWrite(ENA, RPM_TO_PWM(pid_rpm));

	Serial.println(pulse_length);
	Serial.flush();

	// Delay 20ms
	delay(sampling_period);
}
