#-*- coding: utf-8 -*-
import sys
import random
import math
import select
import time
import struct
import serial
import collections
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import numpy as np
import os

sampling_freq = 500

ser = serial.Serial('/dev/ttyACM0',
	baudrate = 115200,
	timeout = None)

def wait_serial():
	c = ''
	while c != '\n':
		c = ser.read(1)

# Toggle DTR to reset Arduino
#ser.setDTR(False)
#ser.flushInput()
#ser.setDTR(True)
time.sleep(2)

def serial_readint():
	s =  ser.readline()
	return int(s[:-2])

def serial_writeint(n):
	return ser.write(str(n) + '\n')

def pulselength_to_rpm(len):
	return (1.0 / ((len / 60e6) * 360.0)) * 2.0

class DynamicPlotter():
	def __init__(self, sampleinterval=0.1, timewindow=10.0):
		# Data stuff
		self.interval = sampleinterval
		self.timewindow = timewindow
		self.numsamples = int(math.ceil(timewindow / sampleinterval))

		# PyQtGraph stuff
		self.app = QtGui.QApplication([])

		self.win = pg.GraphicsWindow()
		self.win.setWindowTitle('STR - Lab 2 - Exercici 1')
		self.plt1 = self.win.addPlot()
		self.plt1.setLabel(axis = "left", text = "Velocity (rpm)")
		self.plt1.setLabel(axis = "bottom", text = "Time (s)")
		self.curve_real_rpm = self.plt1.plot()
		self.curve_desired_rpm = self.plt1.plot()

		# Init variables
		self.desired_rpm = 140

		# Plot data
		self.time = 0.0
		self.data_real_rpm = []
		self.data_desired_rpm = []

		# Time
		self.init_time = time.time()

		# RPM time
		self.rpm_time = self.get_time()

		# Start arduino!
		serial_writeint(self.desired_rpm)

		# QTimer
		self.timer = QtCore.QTimer()
		self.timer.timeout.connect(self.updateplot)
		self.timer.start(self.interval * 1000)

	def get_time(self):
		return time.time() - self.init_time

	def add_sample(self, array, val):
		array.append((self.get_time(), val))
		if len(array) > self.numsamples:
			del array[0]

	def fetchdata(self):
		l = serial_readint()

		print(l)

		rpm = pulselength_to_rpm(l)
		print(rpm)

		print("")

		self.add_sample(self.data_real_rpm, rpm)
		self.add_sample(self.data_desired_rpm, self.desired_rpm)

	def updatedata(self):
		self.fetchdata()

		self.curve_real_rpm.setData(np.array(self.data_real_rpm), pen = pg.mkPen(color=(255, 0, 0), style=QtCore.Qt.SolidLine))
		self.curve_desired_rpm.setData(np.array(self.data_desired_rpm), pen = pg.mkPen(color=(0, 255, 0), style=QtCore.Qt.SolidLine))

		# Actualitzar 'live' plot
		self.plt1.autoRange()
		self.plt1.setXRange(self.get_time() - self.timewindow, self.get_time())

		# Rang de les mostres
		self.plt1.setYRange(100, 200)

		# Update time
		self.time = self.time + self.interval

	def updateplot(self):
		while ser.inWaiting() > 0:
			# Update plot data
			self.updatedata()

			# Update target RPM
			if self.get_time() - self.rpm_time > 1.0:
				if self.desired_rpm == 120:
					self.desired_rpm = 140
				else:
					self.desired_rpm = 120

				self.rpm_time = self.get_time()

			# Send desired RPMs to the Arduino
			serial_writeint(self.desired_rpm)

			self.app.processEvents()

	def run(self):
		self.app.exec_()

if __name__ == '__main__':
	interval = (1.0 / sampling_freq)
	plot = DynamicPlotter(sampleinterval=interval, timewindow=5.0)
	plot.run()
	ser.close()
