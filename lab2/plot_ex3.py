#-*- coding: utf-8 -*-
import sys
import random
import math
import select
import time
import struct
import serial
import collections
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import numpy as np
import os

sampling_freq = 1000

ser = serial.Serial('/dev/ttyACM0',
	baudrate = 115200,
	timeout = None)

def wait_serial():
	c = ''
	while c != '\n':
		c = ser.read(1)

# Toggle DTR to reset Arduino
#ser.setDTR(False)
#ser.flushInput()
#ser.setDTR(True)
time.sleep(2)

def serial_readint():
	s =  ser.readline()
	return int(s[:-2])

def serial_writeint(n):
	return ser.write((str(n) + '\n').encode())

def pulselength_to_rpm(len):
	return (1.0 / ((len / 60e6) * 360.0)) * 2.0

class DynamicPlotter(QtGui.QWidget):
	def __init__(self, sampleinterval=0.1, timewindow=10.0):
		super(DynamicPlotter, self).__init__()

		# Data stuff
		self.interval = sampleinterval
		self.timewindow = timewindow
		self.numsamples = int(math.ceil(timewindow / sampleinterval))

		# Init variables
		self.desired_rpm = 140
		self.sampling_period = 20

		# Qt stuff
		self.vertical_layout = QtGui.QVBoxLayout(self)
		self.horizontal_layout = QtGui.QHBoxLayout()

		# Add spinbox and label
		self.label_sampling_period = QtGui.QLabel('Sampling period (ms):')
		self.horizontal_layout.addWidget(self.label_sampling_period)
		self.spinbox_sampling_period = QtGui.QSpinBox(self)
		self.spinbox_sampling_period.setMinimum(5)
		self.spinbox_sampling_period.setMaximum(1000)
		self.spinbox_sampling_period.setValue(self.sampling_period)
		self.spinbox_sampling_period.valueChanged.connect(self.spinbox_sampling_period_changed)
		self.horizontal_layout.addWidget(self.spinbox_sampling_period)

		self.vertical_layout.addLayout(self.horizontal_layout)

		# Add plot window
		self.plot_win = pg.GraphicsWindow(title="STR")
		self.vertical_layout.addWidget(self.plot_win)

		self.plt1 = self.plot_win.addPlot()
		self.plt1.setLabel(axis = "left", text = "Velocity (rpm)")
		self.plt1.setLabel(axis = "bottom", text = "Time (s)")
		self.curve_real_rpm = self.plt1.plot()
		self.curve_desired_rpm = self.plt1.plot()

		# Plot data
		self.time = 0.0
		self.data_real_rpm = []
		self.data_desired_rpm = []

		# Time
		self.init_time = time.time()

		# RPM time
		self.rpm_time = self.get_time()

		# Start arduino!
		serial_writeint(self.desired_rpm)
		serial_writeint(self.sampling_period)

		# QTimer
		self.timer = QtCore.QTimer()
		self.timer.timeout.connect(self.updateplot)
		self.timer.start(self.interval * 1000)

		# Show widget!
		self.show()

	def get_time(self):
		return time.time() - self.init_time

	def add_sample(self, array, val):
		array.append((self.get_time(), val))
		if len(array) > self.numsamples:
			del array[0]

	def spinbox_sampling_period_changed(self):
		self.sampling_period = self.spinbox_sampling_period.value()
		self.numsamples = int(math.ceil(self.timewindow / (self.sampling_period / 1000.0)))

	def fetchdata(self):
		l = serial_readint()

		print(l)

		rpm = pulselength_to_rpm(l)
		print(rpm)

		print("")

		self.add_sample(self.data_real_rpm, rpm)
		self.add_sample(self.data_desired_rpm, self.desired_rpm)

	def updatedata(self):
		self.fetchdata()

		self.curve_real_rpm.setData(np.array(self.data_real_rpm), pen = pg.mkPen(color=(255, 0, 0), style=QtCore.Qt.SolidLine))
		self.curve_desired_rpm.setData(np.array(self.data_desired_rpm), pen = pg.mkPen(color=(0, 255, 0), style=QtCore.Qt.SolidLine))

		# Actualitzar 'live' plot
		self.plt1.autoRange()
		self.plt1.setXRange(self.get_time() - self.timewindow, self.get_time())

		# Rang de les mostres
		self.plt1.setYRange(100, 200)

		# Update time
		self.time = self.time + self.interval

	def updateplot(self):
		while ser.inWaiting() > 0:
			# Update plot data
			self.updatedata()

			# Update target RPM
			if self.get_time() - self.rpm_time > 1.0:
				if self.desired_rpm == 120:
					self.desired_rpm = 140
				else:
					self.desired_rpm = 120

				self.rpm_time = self.get_time()

			# Send desired RPMs to the Arduino
			serial_writeint(self.desired_rpm)
			# Send sampling period
			serial_writeint(self.sampling_period)


	def run(self):
		self.exec_()

if __name__ == '__main__':
	interval = (1.0 / sampling_freq)

	app = QtGui.QApplication(sys.argv)
	plot = DynamicPlotter(sampleinterval=interval, timewindow=5.0)
	sys.exit(app.exec_())
	ser.close()
