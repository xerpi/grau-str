#include <Wire.h>
#include <SPI.h>
#include <Ethernet.h>
#include <EthernetUdp.h> // UDP library

/* Motor defines */
#define RPM_TO_PWM(x) (((x) * 255ul) / 220)
#define PULSELENGTH_TO_RPM(x) (1000000ul / ((x) * 3ul))
#define ANGLE_TO_RPM(x) (((x) * 220ul) / 90ul)
#define ENA 6
#define IN1 7
#define IN2 8
#define ENCODER_B 2
#define ENCODER_A 3

/* Sensor defines */
#define MPU 0x68 // I2C address of the MPU-6050
#define TAU 0.075

/* UDP defines */
#define LOCAL_PORT 8888
#define DEST_PORT 8890

/* Motor variables */
static long sampling_period = 20;
static long target_rpm = 120;

/* UDP variables */
static uint8_t mac_address[] = {0xCA, 0xFE, 0xD0, 0x0D, 0xD0, 0x0D};
static IPAddress local_ip(192, 168, 1, 201);
static IPAddress destination_ip(192, 168, 1, 203);
static EthernetUDP udp;

// Timekeeping
static unsigned long last_millis = 0;
// PID values
static float Kp = 0.5f;
static float Ki = 0.5f;

void setup()
{
	/* Serial for debugging */
	Serial.begin(115200);

	/* Motor setup */
	pinMode(ENA, OUTPUT);
	pinMode(IN1, OUTPUT);
	pinMode(IN2, OUTPUT);

	pinMode(ENCODER_A, INPUT);
	pinMode(ENCODER_B, INPUT);

	digitalWrite(IN1, HIGH);
	digitalWrite(IN2, LOW);

	/* Start motor */
	analogWrite(ENA, RPM_TO_PWM(target_rpm));
	digitalWrite(ENCODER_B, HIGH);

	/* MPU setup */
	Wire.begin();
	Wire.beginTransmission(MPU);
	Wire.write(0x6B); // PWR_MGMT_1 register
	Wire.write(0); // set to zero (wakes up the MPU-6050)
	Wire.endTransmission(true);

	/* UDP setup */
	Ethernet.begin(mac_address, local_ip);
	udp.begin(LOCAL_PORT);

	last_millis = millis();
}

static void udp_send_angle(int angle)
{
	static char buffer[32];
	int n;

	n = snprintf(buffer, sizeof(buffer), "%d", angle);

	udp.beginPacket(destination_ip, DEST_PORT);
	udp.write(buffer, n + 1);
	udp.endPacket();
}

static int udp_recv_angle(int *angle)
{
	static char buffer[UDP_TX_PACKET_MAX_SIZE];

	int packet_size = udp.parsePacket();
	if (!packet_size)
		return 0;

	udp.read(buffer, sizeof(buffer));

	*angle = atoi(buffer);

	return 1;
}

static int sensor_get_angle(unsigned long delta_millis)
{
	static int old_angle = 0;
	int angle;
	int16_t acc_x, acc_z, gyro_y;
	float pitch;
	float alpha;

	/* Read sensor data */
	Wire.beginTransmission(MPU);
	Wire.write(0x3B);			// starting with register 0x3B (ACCEL_XOUT_H)
	Wire.endTransmission(false);
	Wire.requestFrom(MPU, 12, true);	// request a total of 14 registers

	acc_x = Wire.read() << 8 | Wire.read();		// 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
	Wire.read(); Wire.read();			// 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
	acc_z = Wire.read() << 8 | Wire.read();		// 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
	Wire.read(); Wire.read();			// 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
	Wire.read(); Wire.read();			// 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
	gyro_y = Wire.read() << 8 | Wire.read();	// 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)

	/* calculate pitch (Y-axis rotation) */
	pitch = atan2(acc_x, acc_z) * RAD_TO_DEG;

	/* Apply complementary filter */
	alpha = TAU / (TAU + delta_millis);
	angle = alpha * (old_angle + gyro_y * (delta_millis / 1000.0f)) + (1 - alpha) * pitch;

	old_angle = angle;

	return angle;
}

static void control_motor(unsigned long delta_millis)
{
	static long accumulated_error = 0;
	unsigned long pulse_length;
	unsigned long current_rpm;
	long error_rpm;
	unsigned long pid_rpm;

	// Read motor's RPM
	pulse_length = pulseIn(ENCODER_B, HIGH);

	current_rpm = PULSELENGTH_TO_RPM(pulse_length);

	// PID controller
	error_rpm = (long)target_rpm - current_rpm;
	accumulated_error += error_rpm * delta_millis;
	pid_rpm = Kp * error_rpm + Ki * (accumulated_error / 10.0f);

	// Write RPM correction to motor
	analogWrite(ENA, RPM_TO_PWM(pid_rpm));
}

void loop()
{
	unsigned long cur_millis;
	unsigned long delta_millis;

	// Delta time between now and last iteration
	cur_millis = millis();
	delta_millis = cur_millis - last_millis;
	last_millis = cur_millis;

	int sensor_angle = sensor_get_angle(delta_millis);
	if (sensor_angle < 0)
		sensor_angle = 0;
	else if (sensor_angle > 90)
		sensor_angle = 90;
	udp_send_angle(sensor_angle);

	int udp_angle;
	if (udp_recv_angle(&udp_angle)) {
		int angle = udp_angle;
		if (angle < 0)
			angle = 0;
		else if (angle > 90)
			angle = 90;
		target_rpm = ANGLE_TO_RPM(angle);

		Serial.print("RECV: ");
		Serial.println(udp_angle);
		Serial.print("SEND: ");
		Serial.println(sensor_angle);
		Serial.println("");
	}

	control_motor(delta_millis);
}
