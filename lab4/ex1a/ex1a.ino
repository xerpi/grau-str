#include <Wire.h>
#include <SPI.h>
#include <Ethernet.h>
#include <EthernetUdp.h> // UDP library

/* Motor defines */
#define RPM_TO_PWM(x) (((x) * 255ul) / 220)
#define PULSELENGTH_TO_RPM(x) (1000000ul / ((x) * 3ul))
#define ANGLE_TO_RPM(x) (((x) * 220ul) / 90ul)
#define ENA 6
#define IN1 7
#define IN2 8
#define ENCODER_B 2
#define ENCODER_A 3

/* Sensor defines */
#define MPU 0x68 // I2C address of the MPU-6050
#define TAU 0.075

/* UDP defines */
#define LOCAL_PORT 8888
#define DEST_PORT 8888

/* Peer defines */
#define PEER_ID		5
#define NUM_PEERS	5 /* Without counting ourselves */

typedef struct {
	uint8_t id;
	uint8_t rpm;
} __attribute__((packed)) Packet;

/* Motor variables */
static long sampling_period = 20;

/* UDP variables */
static uint8_t mac_address[] = {0xCA, 0xFE, 0xD0, 0x0D, 0xD0, 0x0D};
static IPAddress local_ip(192, 168, 1, 201);
static IPAddress broadcast_ip(192, 168, 1, 255);
static EthernetUDP udp;

// Timekeeping
static unsigned long last_millis = 0;
// PID values
static float Kp = 0.8f;
static float Ki = 0.2f;

/* Global variables shared among tasks */
static long target_rpm = 80;

/* Data to monitor */
static long local_rpm = 0;
static long average_rpm = 0;
static long current_rpm = 0;
static long num_contributors = 0;

static inline void packet_broadcast(const Packet *p)
{
	udp.beginPacket(broadcast_ip, DEST_PORT);
	udp.write((uint8_t *)p, sizeof(*p));
	udp.endPacket();
}

static inline void packet_read(Packet *p)
{
	udp.read((uint8_t *)p, sizeof(*p));
}

void setup()
{
	/* Serial for debugging */
	Serial.begin(115200);

	/* Motor setup */
	pinMode(ENA, OUTPUT);
	pinMode(IN1, OUTPUT);
	pinMode(IN2, OUTPUT);

	pinMode(ENCODER_A, INPUT);
	pinMode(ENCODER_B, INPUT);

	digitalWrite(IN1, HIGH);
	digitalWrite(IN2, LOW);

	/* Start motor */
	analogWrite(ENA, RPM_TO_PWM(target_rpm));
	digitalWrite(ENCODER_B, HIGH);

	/* MPU setup */
	Wire.begin();
	Wire.beginTransmission(MPU);
	Wire.write(0x6B); // PWR_MGMT_1 register
	Wire.write(0); // set to zero (wakes up the MPU-6050)
	Wire.endTransmission(true);

	/* UDP setup */
	Ethernet.begin(mac_address, local_ip);
	udp.begin(LOCAL_PORT);

	last_millis = millis();
}

static inline unsigned long update_micros(unsigned long *last_micros)
{
	unsigned long cur_micros = micros();
	unsigned long delta_micros = cur_micros - *last_micros;
	*last_micros = cur_micros;
	return delta_micros;
}

#define wait_for_delta_time(start_micros, target_delta) \
	do { \
	} while ((micros() - start_micros) < target_delta)

#define TIMING_START \
	unsigned long start_micros = micros();

#define TIMING_END \
	unsigned long elapsed_micros = micros() - start_micros; \
	Serial.print(__FUNCTION__); \
	Serial.print(": "); \
	Serial.println(elapsed_micros);

void TASK_motor_control()
{
	static unsigned long last_micros = 0;
	static long accumulated_error = 0;
	unsigned long delta_micros;
	unsigned long pulse_length;
	long error_rpm;
	long pid_rpm;
	unsigned long pwm;

	// TIMING_START

	delta_micros = update_micros(&last_micros);

	// Read motor's RPM
	pulse_length = pulseIn(ENCODER_B, HIGH);
	current_rpm = PULSELENGTH_TO_RPM(pulse_length);

	// PID controller
	error_rpm = (long)average_rpm - current_rpm;
	accumulated_error += error_rpm * (delta_micros / 1000.0f);
	pid_rpm = Kp * error_rpm + Ki * (accumulated_error / 1000.0f);
	if (pid_rpm < 40)
		pid_rpm = 40;
	pwm = RPM_TO_PWM(pid_rpm);
	if (pwm > 255)
		pwm = 255;

	// Write RPM correction to motor
	analogWrite(ENA, pwm);

	// TIMING_END

}

void TASK_generate_and_send_local_speed()
{
	static unsigned long last_micros = 0;
	unsigned long delta_micros;
	static int old_angle = 0;
	int angle;
	int16_t acc_x, acc_z, gyro_y;
	float pitch;
	float alpha;
	Packet p;

	// TIMING_START

	delta_micros = update_micros(&last_micros);

	/* Read sensor data */
	Wire.beginTransmission(MPU);
	Wire.write(0x3B);			// starting with register 0x3B (ACCEL_XOUT_H)
	Wire.endTransmission(false);
	Wire.requestFrom(MPU, 12, true);	// request a total of 14 registers

	acc_x = Wire.read() << 8 | Wire.read();		// 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
	Wire.read(); Wire.read();			// 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
	acc_z = Wire.read() << 8 | Wire.read();		// 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
	Wire.read(); Wire.read();			// 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
	Wire.read(); Wire.read();			// 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
	gyro_y = Wire.read() << 8 | Wire.read();	// 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)

	/* calculate pitch (Y-axis rotation) */
	pitch = atan2(acc_x, acc_z) * RAD_TO_DEG;

	/* Apply complementary filter */
	alpha = TAU / (TAU + (delta_micros / 1000.0f));
	angle = alpha * (old_angle + gyro_y * (delta_micros / 1000000.0f)) + (1 - alpha) * pitch;

	old_angle = angle;

	if (angle < 0)
		angle = 0;
	else if (angle > 90)
		angle = 90;
	local_rpm = ANGLE_TO_RPM(angle);
	if (local_rpm < 50)
		local_rpm = 50;

	p.id = PEER_ID;
	p.rpm = local_rpm;
	packet_broadcast(&p);

	// TIMING_END
}

void TASK_receive_and_average_speed()
{
	int recv_cnt;
	uint8_t recv_mask;

	// TIMING_START

	average_rpm = 0;
	num_contributors = 0;
	recv_cnt = 0;
	recv_mask = 0;

	/*
	 * Try to read a packet for each peer.
	 */
	while (recv_cnt < NUM_PEERS) {
		Packet p;
		int packet_size = udp.parsePacket();

		if (packet_size != sizeof(p))
			continue;

		recv_cnt++;

		packet_read(&p);

		/*
		 * If we have already received a packet
		 * from this peer, discard it.
		 */
		if (recv_mask & (1 << p.id))
			continue;

		recv_mask |= 1 << p.id;
		average_rpm += p.rpm;

		num_contributors++;
	}

	average_rpm /= num_contributors;

	// TIMING_END
}

void TASK_monitor()
{
	// TIMING_START

	Serial.print("local:");
	Serial.println(local_rpm);

	Serial.print("average:");
	Serial.println(average_rpm);

	Serial.print("current:");
	Serial.println(current_rpm);

	Serial.print("num_contributors:");
	Serial.println(num_contributors);

	Serial.flush();

	// TIMING_END
}

#define MINOR_CYCLE_DURATION 20000

#define minor_cycle(task1, task2) \
	minor_cycle_start_time = micros(); \
	task1(); \
	task2(); \
	wait_for_delta_time(minor_cycle_start_time, MINOR_CYCLE_DURATION);

void task_noop()
{
}

void loop()
{
	unsigned long minor_cycle_start_time;

	minor_cycle(TASK_monitor, TASK_motor_control);
	minor_cycle(TASK_monitor, TASK_generate_and_send_local_speed);
	minor_cycle(TASK_monitor, TASK_motor_control);
	minor_cycle(TASK_monitor, TASK_receive_and_average_speed);
	minor_cycle(TASK_monitor, TASK_motor_control);
	minor_cycle(TASK_monitor, TASK_generate_and_send_local_speed);
	minor_cycle(TASK_monitor, TASK_motor_control);
	minor_cycle(TASK_monitor, TASK_receive_and_average_speed);
	minor_cycle(TASK_monitor, TASK_motor_control);
	minor_cycle(TASK_monitor, task_noop);
}
