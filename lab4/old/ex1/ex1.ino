#include <NilRTOS.h>

const uint8_t LED_PIN = 13;

NIL_WORKING_AREA(waThread1, 128);

NIL_THREAD(Thread1, arg) {
	while (TRUE) {
		nilThdSleepMilliseconds(200);
		digitalWrite(LED_PIN, LOW);
		nilThdSleepMilliseconds(200);
	}
}

NIL_WORKING_AREA(waThread2, 128);

NIL_THREAD(Thread2, arg) {

	pinMode(LED_PIN, OUTPUT);

	while (TRUE) {
		// Turn LED on.
		digitalWrite(LED_PIN, HIGH);
		// Sleep for 200 milliseconds.
		nilThdSleepMilliseconds(200);
		// Sleep for 200 milliseconds.
		nilThdSleepMilliseconds(200);
	}
}

NIL_THREADS_TABLE_BEGIN()
NIL_THREADS_TABLE_ENTRY("thread1", Thread1, NULL, waThread1, sizeof(waThread1))
NIL_THREADS_TABLE_ENTRY("thread2", Thread2, NULL, waThread2, sizeof(waThread2))
NIL_THREADS_TABLE_END()

void setup() {
	// Start Nil RTOS.
	nilSysBegin();
}

void loop() {
	// Not used.
}
