static uint32_t gcd(uint32_t a, uint32_t b);

void TASK_motor_control()
{
	Serial.println(__FUNCTION__);
}

void TASK_generate_and_send_local_speed()
{
	Serial.println(__FUNCTION__);
}

void TASK_receive_and_average_speed()
{
	Serial.println(__FUNCTION__);
}

void TASK_monitor()
{
	Serial.println(__FUNCTION__);
}

static struct {
	void (*fn)(void);
	uint32_t period;
	uint32_t num;
} tasks[] = {
	{TASK_motor_control, 20, 0},
	{TASK_generate_and_send_local_speed, 100, 0},
	{TASK_receive_and_average_speed, 100, 0},
	{TASK_monitor, 10, 0}
};

#define NUM_TASKS (sizeof(tasks) / (sizeof(*tasks)))

uint32_t minor_cycle_period;
uint32_t minor_cycle_count;

void setup()
{
	minor_cycle_period = tasks[0].period;
	for (int i = 1; i < NUM_TASKS; i++) {
		minor_cycle_period = gcd(minor_cycle_period, tasks[i].period);
	}

	for (int i = 0; i < NUM_TASKS; i++) {
		tasks[i].num = tasks[i].period / minor_cycle_period;
	}

	minor_cycle_count = 0;

	Serial.begin(115200);
}

void loop()
{
	uint32_t start_time = millis();

	for (int i = 0; i < NUM_TASKS; i++) {
		if (minor_cycle_count % tasks[i].num == 0) {
			tasks[i].fn();
		}
	}

	while (millis() < start_time + minor_cycle_period)
		;

	minor_cycle_count++;
}

static uint32_t gcd(uint32_t a, uint32_t b)
{
	uint32_t c;
	while (a != 0) {
		c = a;
		a = b % a;
		b = c;
	}
	return b;
}
