#include <Wire.h>

/* Motor defines */
#define RPM_TO_PWM(x) (((x) * 255ul) / 220)
#define PULSELENGTH_TO_RPM(x) (1000000ul / ((x) * 3ul))
#define ANGLE_TO_RPM(x) (((x) * 220ul) / 90ul)
#define ENA 6
#define IN1 7
#define IN2 8
#define ENCODER_B 2
#define ENCODER_A 3

/* Sensor defines */
#define MPU 0x68 // I2C address of the MPU-6050
#define TAU 0.075

static long target_rpm = 100;
static unsigned long current_rpm = target_rpm;

// Timekeeping
static unsigned long last_micros = 0;
static unsigned long isr_last_micros = 0;
// PID values
static float Kp = 0.2f;
static float Ki = 0.8f;
// Accumulated error
static long accumulated_error = 0;

static void control_motor(unsigned long delta_micros)
{
	static long accumulated_error = 0;
	unsigned long pulse_length;
	long error_rpm;
	unsigned long pid_rpm;
	unsigned long pwm;

	// Read motor's RPM
	pulse_length = pulseIn(ENCODER_B, HIGH);
	current_rpm = PULSELENGTH_TO_RPM(pulse_length);

	//Serial.println(current_rpm);

	// PID controller
	error_rpm = (long)target_rpm - current_rpm;
	accumulated_error += error_rpm * delta_micros;
	pid_rpm = Kp * error_rpm + Ki * (accumulated_error / 1000000.0f);
	pwm = RPM_TO_PWM(pid_rpm);
	if (pwm > 255)
		pwm = 255;

	// Write RPM correction to motor
	analogWrite(ENA, pwm);
}

static int sensor_get_angle(unsigned long delta_micros)
{
	static int old_angle = 0;
	int angle;
	int16_t acc_x, acc_z, gyro_y;
	float pitch;
	float alpha;

	/* Read sensor data */
	Wire.beginTransmission(MPU);
	Wire.write(0x3B);			// starting with register 0x3B (ACCEL_XOUT_H)
	Wire.endTransmission(false);
	Wire.requestFrom(MPU, 12, true);	// request a total of 14 registers

	acc_x = Wire.read() << 8 | Wire.read();		// 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
	Wire.read(); Wire.read();			// 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
	acc_z = Wire.read() << 8 | Wire.read();		// 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
	Wire.read(); Wire.read();			// 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
	Wire.read(); Wire.read();			// 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
	gyro_y = Wire.read() << 8 | Wire.read();	// 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)

	/* calculate pitch (Y-axis rotation) */
	pitch = atan2(acc_x, acc_z) * RAD_TO_DEG;

	/* Apply complementary filter */
	alpha = TAU / (TAU + (delta_micros / 1000.0f));
	angle = alpha * (old_angle + gyro_y * (delta_micros / 1000000.0f)) + (1 - alpha) * pitch;

	old_angle = angle;

	return angle;
}

void setup()
{
	/* Serial for debugging */
	Serial.begin(115200);

	/* Motor setup */
	pinMode(ENA, OUTPUT);
	pinMode(IN1, OUTPUT);
	pinMode(IN2, OUTPUT);

	pinMode(ENCODER_A, INPUT);
	pinMode(ENCODER_B, INPUT);

	digitalWrite(IN1, HIGH);
	digitalWrite(IN2, LOW);

	/* Start motor */
	analogWrite(ENA, RPM_TO_PWM(target_rpm));
	digitalWrite(ENCODER_B, HIGH);

	/* MPU setup */
	Wire.begin();
	Wire.beginTransmission(MPU);
	Wire.write(0x6B); // PWR_MGMT_1 register
	Wire.write(0); // set to zero (wakes up the MPU-6050)
	Wire.endTransmission(true);

	last_micros = micros();
	isr_last_micros = micros();
}

void loop()
{
	unsigned long cur_micros;
	unsigned long delta_micros;
	unsigned long pulse_length;
	unsigned long current_rpm;
	long error_rpm;
	unsigned long pid_rpm;

	// Delta time between now and last iteration
	cur_micros = micros();
	delta_micros = cur_micros - last_micros;
	last_micros = cur_micros;

	//target_rpm = Serial.parseInt();
	int sensor_angle = sensor_get_angle(delta_micros);

	//Serial.println(sensor_angle);

	if (sensor_angle < 0)
		sensor_angle = 0;
	else if (sensor_angle > 90)
		sensor_angle = 90;
	target_rpm = ANGLE_TO_RPM(sensor_angle);

	control_motor(delta_micros);

	//Serial.println(pulse_length);
	//Serial.flush();

	// Delay 20ms
	//delay(20);
}
