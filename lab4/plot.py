#-*- coding: utf-8 -*-
import sys
import random
import math
import select
import time
import struct
import serial
import collections
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import numpy as np
import os

sampling_freq = 10000

ser = serial.Serial('/dev/ttyACM1',
	baudrate = 115200,
	timeout = None)

def wait_serial():
	c = ''
	while c != '\n':
		c = ser.read(1)

# Toggle DTR to reset Arduino
#ser.setDTR(False)
#ser.flushInput()
#ser.setDTR(True)
time.sleep(2)

def serial_readint():
	s =  ser.readline()
	return int(s[:-2])

def serial_writeint(n):
	return ser.write((str(n) + '\n').encode())

def pulselength_to_rpm(len):
	return (1.0 / ((len / 60e6) * 360.0)) * 2.0

class DynamicPlotter(QtGui.QWidget):
	def __init__(self, sampleinterval=0.1, timewindow=10.0):
		super(DynamicPlotter, self).__init__()

		# Data stuff
		self.interval = sampleinterval
		self.timewindow = timewindow
		self.numsamples = int(math.ceil(timewindow / sampleinterval))

		# Init variables
		self.desired_rpm = 140
		self.sampling_period = 20

		# Qt stuff
		self.vertical_layout = QtGui.QVBoxLayout(self)

		# Add plot window
		self.plot_win1 = pg.GraphicsWindow(title="STR")
		self.plot_win2 = pg.GraphicsWindow(title="STR")
		self.vertical_layout.addWidget(self.plot_win1)
		self.vertical_layout.addWidget(self.plot_win2)

		self.plt1 = self.plot_win1.addPlot()
		self.plt1.setLabel(axis = "left", text = "Velocity (rpm)")
		self.plt1.setLabel(axis = "bottom", text = "Time (s)")
		self.curve_local_rpm = self.plt1.plot()
		self.curve_average_rpm = self.plt1.plot()
		self.curve_current_rpm = self.plt1.plot()

		self.plt2 = self.plot_win2.addPlot()
		self.plt2.setLabel(axis = "left", text = "Number of contributors")
		self.plt2.setLabel(axis = "bottom", text = "Time (s)")
		self.curve_num_contributors = self.plt2.plot()

		# Plot data
		self.time = 0.0
		self.data_local_rpm = []
		self.data_average_rpm = []
		self.data_current_rpm = []
		self.data_num_contributors = []

		# Time
		self.init_time = time.time()

		# RPM time
		self.rpm_time = self.get_time()

		# Start arduino!
		serial_writeint(self.desired_rpm)
		serial_writeint(self.sampling_period)

		# QTimer
		self.timer = QtCore.QTimer()
		self.timer.timeout.connect(self.updateplot)
		self.timer.start(self.interval * 1000)

		# Show widget!
		self.show()

	def get_time(self):
		return time.time() - self.init_time

	def add_sample(self, array, val):
		array.append((self.get_time(), val))
		if len(array) > self.numsamples:
			del array[0]

	def spinbox_sampling_period_changed(self):
		self.sampling_period = self.spinbox_sampling_period.value()
		self.numsamples = int(math.ceil(self.timewindow / (self.sampling_period / 1000.0)))

	def fetchdata(self):
		local_rpm = 0
		average_rpm = 0
		current_rpm = 0
		num_contributors = 0

		for i in range(4):
			try:
				data = ser.readline().decode("utf-8").strip().split(":")
				if data[0] == "local":
					local_rpm = int(data[1])
				elif data[0] == "average":
					average_rpm = int(data[1])
				elif data[0] == "current":
					current_rpm = int(data[1])
				elif data[0] == "num_contributors":
					num_contributors = int(data[1])
			except:
				print("Error reading serial data...")

		print(local_rpm)
		print(average_rpm)
		print(current_rpm)
		print(num_contributors)

		self.add_sample(self.data_local_rpm, local_rpm)
		self.add_sample(self.data_average_rpm, average_rpm)
		self.add_sample(self.data_current_rpm, current_rpm)
		self.add_sample(self.data_num_contributors, num_contributors)

	def updatedata(self):
		self.fetchdata()

		self.curve_local_rpm.setData(np.array(self.data_local_rpm), pen = pg.mkPen(color=(255, 0, 0), style=QtCore.Qt.SolidLine))
		self.curve_average_rpm.setData(np.array(self.data_average_rpm), pen = pg.mkPen(color=(0, 255, 0), style=QtCore.Qt.SolidLine))
		self.curve_current_rpm.setData(np.array(self.data_current_rpm), pen = pg.mkPen(color=(0, 0, 255), style=QtCore.Qt.SolidLine))
		self.curve_num_contributors.setData(np.array(self.data_num_contributors), pen = pg.mkPen(color=(255, 255, 255), style=QtCore.Qt.SolidLine))

		# Actualitzar 'live' plot
		self.plt1.autoRange()
		self.plt1.setXRange(self.get_time() - self.timewindow, self.get_time())
		# Rang de les mostres
		self.plt1.setYRange(0, 220)

		# Actualitzar 'live' plot
		self.plt2.autoRange()
		self.plt2.setXRange(self.get_time() - self.timewindow, self.get_time())
		# Rang de les mostres
		self.plt2.setYRange(0, 6)

		# Update time
		self.time = self.time + self.interval

	def updateplot(self):
		if ser.inWaiting() > 0:
			# Update plot data
			self.updatedata()
			self.repaint()

	def run(self):
		self.exec_()

if __name__ == '__main__':
	interval = (1.0 / sampling_freq)

	app = QtGui.QApplication(sys.argv)
	plot = DynamicPlotter(sampleinterval=interval, timewindow=5.0)
	sys.exit(app.exec_())
	ser.close()
