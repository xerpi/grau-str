/*
 * Autors:
 *     Sergi Granell Escalfet
 *     Marc Perelló Bacardit
 */

#include <mcp_can.h>
#include <SPI.h>
#include "seeds.h"

#define PLAYER_ID 4
#define NUM_PLAYERS 5
#define NUM_ROUNDS 4

#define print_int(s, n) \
	do { \
		Serial.print(s); \
		Serial.println(n); \
	} while (0)

#define print_int2(s1, n1, s2, n2) \
	do { \
		Serial.print(s1); \
		Serial.print(n1); \
		Serial.print(s2); \
		Serial.println(n2); \
	} while (0)

MCP_CAN CAN(10);

enum CroupierIndication {
	IN1 = 10,
	IN2 = 20,
	IN3 = 30,
	IN4 = 40
};

enum Operator {
	ADD = 0,
	SUB = 1,
	MUL = 2,
	XOR = 3
};

void can_init();

bool can_recv(INT8U buffer[8], INT32U *recv_id, INT8U *recv_len)
{
	if (CAN_MSGAVAIL == CAN.checkReceive()) {
		CAN.readMsgBufID(recv_id, recv_len, buffer);
		return true;
	}
	return false;
}

bool can_send_packet(INT8U msg_id, INT8U data)
{
	return CAN.sendMsgBuf(msg_id + PLAYER_ID, 0, sizeof(data), &data) == CAN_OK;
}

bool wait_croupier(INT8U msg_id, INT8U *data)
{
	INT32U recv_id;
	INT8U recv_len;
	INT8U recv_buf[8];
	bool ret;

	do {
		ret = can_recv(recv_buf, &recv_id, &recv_len);
		if (ret) {
			print_int2("Waiting for ID: ", msg_id, ", received ID: ", recv_id);
			if ((recv_id - PLAYER_ID) == IN1)
				return false;
		}
	} while (!ret || (recv_id - PLAYER_ID) != msg_id);

	if (data)
		*data = recv_buf[0];

	return true;
}

static INT8U op_add(INT8U data, INT8U key)
{
	return data + key;
}

static INT8U op_sub(INT8U data, INT8U key)
{
	return data - key;
}

static INT8U op_mul(INT8U data, INT8U key)
{
	return data * key;
}

static INT8U op_xor(INT8U data, INT8U key)
{
	return data ^ key;
}

static const INT8U (*op_table[])(INT8U, INT8U) = {
	[ADD] = op_add,
	[SUB] = op_sub,
	[MUL] = op_mul,
	[XOR] = op_xor
};

INT8U encrypt_data(INT8U data, INT8U key, Operator op)
{
	return op_table[op](data, key);
}

INT8U random_hand()
{
	return random(0, 4);
}

INT8U random_guess()
{
	return random(0, 16);
}

INT8U calculate_guess(INT8U our_hand)
{
	// formula: rand((hand + 1.5 * 4) - 2, (hand + 1.5 * 4) + 2)
	return random(our_hand + 4, our_hand + 8 + 1);
}

bool game_round(bool skip_wait_in1)
{
	INT8U hand, encrypted_hand;
	INT8U initial_guess;
	INT8U final_guess;

	randomSeed(analogRead(0) * analogRead(0));

	Serial.println("-------- ROUND START --------");

	if (!skip_wait_in1)
		wait_croupier(IN1, NULL); // IN1
	can_send_packet(IN1, 0); // IN1 response, ACK

	if (!wait_croupier(IN2, NULL)) // IN2
		return false;
	hand = random_hand();
	print_int("Hand is: ", hand);
	encrypted_hand = encrypt_data(hand, KEY, OP); // Encrypted number of coins
	can_send_packet(IN2, encrypted_hand); // IN1 response

	if (!wait_croupier(IN3, NULL)) // IN3
		return false;
	initial_guess = random_guess(); // Initial guess
	print_int("Initial guess is: ", initial_guess);
	can_send_packet(IN3, initial_guess); // IN3 response

	final_guess = calculate_guess(hand); // Final guess
	print_int("Final guess is: ", final_guess);
	can_send_packet(IN3, final_guess); // IN3 response

	Serial.println("-------- ROUND END --------");

	return true;
}

void game_state_machine()
{
	bool skip_wait_in1 = false;

	for (INT8U i = 0; i < NUM_ROUNDS; i++) {
		if (!game_round(skip_wait_in1)){
			Serial.println("-------- ROUND RESET --------");
			i--;
			skip_wait_in1 = true;
		} else {
			skip_wait_in1 = false;
		}
	}
}

void setup()
{
	Serial.begin(115200);
	can_init();
}

void loop()
{
	game_state_machine();
}

void can_init()
{
START_INIT:
	if (CAN_OK == CAN.begin(CAN_500KBPS)) {
		Serial.println("CAN BUS Shield init ok!");
	} else {
		Serial.println("CAN BUS Shield init fail. Init again");
		delay(100);
		goto START_INIT;
	}

	// No need to use CAN filters

	/*CAN.init_Mask(0, 0, 0x3ff);
	CAN.init_Mask(1, 0, 0x3ff);
	CAN.init_Filt(0, 0, 0x01);
	CAN.init_Filt(1, 0, 0x02);
	CAN.init_Filt(2, 0, 0x03);
	CAN.init_Filt(3, 0, 0x04);
	//CAN.init_Filt(4, 0, 0x05);
	CAN.init_Filt(5, 0, 0x06);*/
}
