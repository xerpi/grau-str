#include <mcp_can.h>
#include <SPI.h>

MCP_CAN CAN(10);

void setup()
{
	Serial.begin(115200);

START_INIT:
	if (CAN_OK == CAN.begin(CAN_50KBPS)) {
		Serial.println("CAN BUS Shield init ok!");
	} else {
		Serial.println("CAN BUS Shield init fail. Init again");
		delay(100);
		goto START_INIT;
	}

	CAN.init_Mask(0, 0, 0x3ff);
	CAN.init_Mask(1, 0, 0x3ff);
	CAN.init_Filt(0, 0, 0x01);
	CAN.init_Filt(1, 0, 0x02);
	CAN.init_Filt(2, 0, 0x03);
	CAN.init_Filt(3, 0, 0x04);
	//CAN.init_Filt(4, 0, 0x05);
	CAN.init_Filt(5, 0, 0x06);
}

unsigned char data[8] = {'D', 'E', 'A', 'D', 'F', 'O', 'O', 'D'};
void loop()
{
	CAN.sendMsgBuf(0x05, 0, 8, data);
	delay(100);

	unsigned char recv_len = 0;
	unsigned char recv_buf[8];

	if (CAN_MSGAVAIL == CAN.checkReceive()) {
		INT32U id;
		CAN.readMsgBufID(&id, &recv_len, recv_buf);

		Serial.print("ID: ");
		Serial.print(id);
		Serial.print(" {");

		for(int i = 0; i < recv_len; i++) {
			Serial.print(recv_buf[i]);
			if (i < recv_len - 1)
				Serial.print(", ");
		}
		Serial.println("}");
	}
}
