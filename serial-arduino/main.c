#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <termios.h>

static void configure_serial(int fd);

int main(int argc, char *argv[])
{
	int fd;

	printf("Arduino serial test\n");

	fd = open("/dev/ttyACM0", O_RDWR | O_NOCTTY);
	if (fd < 0) {
		perror("open");
		return -1;
	}

	configure_serial(fd);

	/*uint8_t gyro_sel = 0;
	uint8_t accel_sel = 0;
	uint8_t sampling_freq = 10;

	write(fd, &gyro_sel, sizeof(gyro_sel));
	write(fd, &accel_sel, sizeof(accel_sel));
	write(fd, &sampling_freq, sizeof(sampling_freq));*/

	while (1) {
	/*add_element(data_acc_x, t, acc_data_to_ms2(serial_readint16()), range_values_acc)
	add_element(data_acc_y, t, acc_data_to_ms2(serial_readint16()), range_values_acc)
	add_element(data_acc_z, t, acc_data_to_ms2(serial_readint16()), range_values_acc)
	add_element(data_gyro_x, t, gyro_data_to_degs(serial_readint16()), range_values_gyro)
	add_element(data_gyro_y, t, gyro_data_to_degs(serial_readint16()), range_values_gyro)
	add_element(data_gyro_z, t, gyro_data_to_degs(serial_readint16()), range_values_gyro)
	add_element(data_temp, t, serial_readint16()/340.00+36.53, range_values_temp)*/

		uint16_t i;
		int n = read(fd, &i, sizeof(i));
		printf("0x%04X - %d\n", i, n);
		//usleep(50 * 10);
	}

	close(fd);

	return 0;
}

void configure_serial(int fd)
{
	/* Set up the control structure */
	struct termios toptions;

	/* Get currently set options for the tty */
	tcgetattr(fd, &toptions);

	/* Set custom options */

	/* 9600 baud */
	cfsetispeed(&toptions, B9600);
	cfsetospeed(&toptions, B9600);
	/* 8 bits, no parity, no stop bits */
	toptions.c_cflag &= ~PARENB;
	toptions.c_cflag &= ~CSTOPB;
	toptions.c_cflag &= ~CSIZE;
	toptions.c_cflag |= CS8;
	/* no hardware flow control */
	toptions.c_cflag &= ~CRTSCTS;
	/* enable receiver, ignore status lines */
	toptions.c_cflag |= CREAD | CLOCAL;
	/* disable input/output flow control, disable restart chars */
	toptions.c_iflag &= ~(IXON | IXOFF | IXANY);
	/* disable canonical input, disable echo,
	disable visually erase chars,
	disable terminal-generated signals */
	toptions.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
	/* disable output processing */
	toptions.c_oflag &= ~OPOST;

	/* wait for 2 characters to come in before read returns */
	/* WARNING! THIS CAUSES THE read() TO BLOCK UNTIL ALL */
	/* CHARACTERS HAVE COME IN! */
	toptions.c_cc[VMIN] = 2;
	/* no minimum time to wait before read returns */
	toptions.c_cc[VTIME] = 0;

	/* commit the options */
	tcsetattr(fd, TCSANOW, &toptions);

	/* Wait for the Arduino to reset */
	usleep(1000*1000);
	/* Flush anything already in the serial buffer */
	tcflush(fd, TCIFLUSH);
}
